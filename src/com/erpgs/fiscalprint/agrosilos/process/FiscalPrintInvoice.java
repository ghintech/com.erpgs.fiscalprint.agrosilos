package com.erpgs.fiscalprint.agrosilos.process;

import java.io.*;
import java.math.BigDecimal;
import java.net.*;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

import org.compiere.acct.DocManager;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.MFactAcct;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MOrder;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.erpgs.fiscalprint.agrosilos.model.MFiscalPrintConf;
import com.erpgs.fiscalprint.agrosilos.model.MInvoiceFiscal;

public class FiscalPrintInvoice extends SvrProcess{
	 private int p_C_Invoice_ID=0;
	 private int p_C_InvoiceTo_ID=0;
	 private int p_C_Doctype_ID=0;
	 private int p_C_Order_ID=0;
	 private int p_C_FiscalPrintConf_ID=0;
	 private String p_ReportType=null;
	 private Date p_Date1=null;
	 private Date p_Date2=null;
	 private MFiscalPrintConf fiscalprint=null;
	 private String date1aux;
	 private String date2aux;
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_FiscalPrintConf_ID"))
				p_C_FiscalPrintConf_ID =  para[i].getParameterAsInt();
			else if (name.equals("C_Invoice_ID")){
				p_C_Invoice_ID =  para[i].getParameterAsInt();
				p_C_InvoiceTo_ID = para[i].getParameter_ToAsInt();				
			}
			else if (name.equals("C_Order_ID"))
				p_C_Order_ID =  para[i].getParameterAsInt();
			else if (name.equals("C_DocType_ID"))
				p_C_Doctype_ID =  para[i].getParameterAsInt();
			else if (name.equals("ReportType"))
				p_ReportType =  para[i].getParameterAsString();
			else if (name.equals("Date")){
				p_Date1 =  (Timestamp)para[i].getParameter();
				p_Date2 =  (Timestamp)para[i].getParameter_To();
				
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		
		// TODO Auto-generated method stub
		fiscalprint=new MFiscalPrintConf(getCtx(), p_C_FiscalPrintConf_ID, get_TrxName());
		MInvoice invoice=null;
		MOrder order=null;
		List<MInvoice> invoices = null;
		if(p_C_InvoiceTo_ID !=0 && p_C_Invoice_ID!=0){
			invoices = new Query(getCtx(), MInvoice.Table_Name, "C_Doctype_ID = "+p_C_Doctype_ID+
					" AND DocStatus = 'CO' AND WasPrinted != 'Y' AND C_invoice_ID between "+p_C_Invoice_ID+
					" AND "+p_C_InvoiceTo_ID, get_TrxName()).setClient_ID().setOrderBy("DocumentNo").list();
		}
		else if(p_C_Invoice_ID!=0){
			invoice =new MInvoice(getCtx(), p_C_Invoice_ID, get_TrxName());			
		}
		else if(p_C_Order_ID!=0){
			order=new MOrder(getCtx(),p_C_Order_ID,get_TrxName());
			invoice =new MInvoice(getCtx(), order.getC_Invoice_ID(), get_TrxName());
			MDocType order_doctype= new MDocType(getCtx(), order.getC_DocTypeTarget_ID(), get_TrxName());
			
			if(order_doctype.get_ValueAsInt("C_FiscalPrintConf_ID")>0) {
				fiscalprint = new MFiscalPrintConf(getCtx(), order_doctype.get_ValueAsInt("C_FiscalPrintConf_ID"), get_TrxName());
			}
		}
		boolean check=false;
		if(p_ReportType!=null){
			if(fiscalprint.getFiscalPrintModel().compareTo("H")==0)
				check=HasarZXReport(p_ReportType);
			else if(fiscalprint.getFiscalPrintModel().compareTo("S")==0)
				check=StarZXReport(p_ReportType);;
		}else{
			if(fiscalprint.getFiscalPrintModel().compareTo("H")==0)
				check=HasarInvoice(invoice);
			else if(fiscalprint.getFiscalPrintModel().compareTo("S")==0){
				if(invoices!=null)
					check=StarInvoice(invoices);
				else{
					if(!invoice.get_ValueAsBoolean("WasPrinted"))
						check=StarInvoice(invoice);
				}
					
			}
				
		}
		
		if(check){
		
		 try{
	            FileInputStream fis=new FileInputStream("factura.txt");
			 
	            /*creating a socket to send data to 192.168.1.123
	              i.e. IP address of the machine where the file is to be sent
	              and 10000 is the port number on which server listens  requests*/
	            Socket socket=new Socket(fiscalprint.getURL(),fiscalprint.getPort());
	            OutputStream os=socket.getOutputStream();
	            int ch=0;
	            System.out.println("Sending file  to "+fiscalprint.getURL());
	            while(true){
	 
	                ch=fis.read();
	                if(ch==-1)
	                  break;
	                os.write(ch);
	            }
	            fis.close();
	            os.close();
	            System.out.println("Sending process completed");
	        }
	 
	        catch(Exception e){
	            e.printStackTrace();
	        }
		 	
		}
		
		return "Ok";
	}
	
	private void ProcessStatus(int C_Order_ID,int C_Invoice_ID) {
		// TODO Auto-generated method stub
		try{
			File fileStatus = new File("Status.txt");  
			FileReader reader = new FileReader(fileStatus);
	        BufferedReader br = new BufferedReader(reader); 
	        String status; 
	        while((status = br.readLine()) != null) {
	        	 int zlength=status.substring(47,51).length();
	             int znumber=Integer.parseInt(status.substring(47,51))+1;
	             
	        	MInvoiceFiscal fiscaldata=new MInvoiceFiscal(getCtx(), 0,get_TrxName());
	        	fiscaldata.setC_Order_ID(C_Order_ID);
	        	fiscaldata.setC_Invoice_ID(C_Invoice_ID);
	        	fiscaldata.setfiscal_invoicenumber(status.substring(21,29));
	        	fiscaldata.setfiscalprint_serial(status.substring(66,76));
	        	fiscaldata.setfiscal_zreport(String.format("%0" + String.valueOf(zlength) +"d", znumber));
	        	fiscaldata.saveEx();
	            //logicsale.updateTicketFiscal(ticket,status.substring(21,29),status.substring(66,76),status.substring(47,51));
	        } 
	        reader.close();
		}catch(Exception e){
            e.printStackTrace();
        }
	}

	public static void Receiver(){
		try{
				//we are creating the socket on port 10000
	            ServerSocket server=new ServerSocket(11000);
	            System.out.println("Esperando archivo...");
	            Socket socket=server.accept();
	            InputStream is=socket.getInputStream();
	            FileOutputStream fos=new FileOutputStream("Status.txt");
	            int ch=0;
	            System.out.println("Almacenando contenido en Status.txt");
	            while(true){
	                ch=is.read();
	                if(ch==-1)
	                  break;
	                fos.write(ch);
	            }
	            is.close();
	            fos.close();
	            System.out.println("Creación de archivo completada.");
		    server.close();
		    
	        }
			catch(Exception e){
	            e.printStackTrace();
	       }
	    }
	

	private boolean StarInvoice(MInvoice invoice) {
		// TODO Auto-generated method stub
		/**
		 * 
		 * jSNOMBRE RAZON SOCIAL
			jRRUC_CEDULA
			j3LINEA 3 ENCABEZADO
			j4LINEA 4 ENCABEZADO
			j5LINEA 5 Encabezado
			j6LINEA 6
			@COMENTARIO
			!000000850000001000CHRISTIE`S 3825T/3826T TITANI+ME690
			p-2000
			!000000880000001000PROG. GENERICO
			p-2000
			!000000880000001000PROG. GENERICO
			p-2000
			@prueba de comentarios para que se muestr
			@ en la fiscal
			@  ***GARANTIA***
			101
		 * 
		 */
		
		File file=null;
		//String auxname=null;
		//String auxproductname = null;		
		try{
			file = new File("factura.txt");    
			if(file.exists())
				file.delete();
			file.createNewFile();
		    // creates a FileWriter Object
		    FileWriter writer = new FileWriter(file); 
		    // Writes the content to the file
		    fillWriter(writer, invoice);
		    writer.close();
		    
		}
		catch(Exception e){
			e.printStackTrace();
        }
		
		return true;
	}

	private boolean StarInvoice(List<MInvoice> invoices) {
		// TODO Auto-generated method stub
		/**
		 * 
		 * jSNOMBRE RAZON SOCIAL
			jRRUC_CEDULA
			j3LINEA 3 ENCABEZADO
			j4LINEA 4 ENCABEZADO
			j5LINEA 5 Encabezado
			j6LINEA 6
			@COMENTARIO
			!000000850000001000CHRISTIE`S 3825T/3826T TITANI+ME690
			p-2000
			!000000880000001000PROG. GENERICO
			p-2000
			!000000880000001000PROG. GENERICO
			p-2000
			@prueba de comentarios para que se muestr
			@ en la fiscal
			@  ***GARANTIA***
			101
		 * 
		 */
		
		File file=null;
		//String auxname=null;
		//String auxproductname = null;
		try{
			file = new File("factura.txt");    
			if(file.exists())
				file.delete();
			file.createNewFile();
		    // creates a FileWriter Object
		    FileWriter writer = new FileWriter(file); 
		    // Writes the content to the file
		    for(MInvoice invoice:invoices){
		    	fillWriter(writer, invoice);
			    }
		    writer.close();
		}
		catch(Exception e){
			e.printStackTrace();
        }
		
		return true;
	}
	
	public void fillWriter(FileWriter writer, MInvoice invoice) throws IOException{

	    MBPartner bpartner=new MBPartner(getCtx(), invoice.getC_BPartner_ID(), get_TrxName());
	    MBPartnerLocation bplocation=new MBPartnerLocation(getCtx(),invoice.getC_BPartner_Location_ID(),get_TrxName());
	    invoice.set_ValueOfColumn("WasPrinted", "Y");
	    if(invoice.getDateInvoiced()!=new Timestamp(System.currentTimeMillis())){
	    	invoice.setDateInvoiced(new Timestamp(System.currentTimeMillis()));
	    	invoice.setDateAcct(new Timestamp(System.currentTimeMillis()));
	    	DocManager.postDocument(MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID()), MInvoice.Table_ID, invoice.getC_Invoice_ID(), false, true, get_TrxName());
	    }
	    invoice.saveEx();
	    //auxname = bpartner.getName().replace("&amp;", "&").replace("&quot;", "\"").replace("&apos;", "\'");
	    String Address=" ";
	    if(bplocation.getC_Location().getAddress1()!=null)
	    	Address=bplocation.getC_Location().getAddress1().trim();
	    if(bplocation.getC_Location().getAddress2()!=null)
	    	Address+=" "+bplocation.getC_Location().getAddress2().trim();
	    if(bplocation.getC_Location().getAddress3()!=null)
	    	Address+=" "+bplocation.getC_Location().getAddress3().trim();
	    if(bplocation.getC_Location().getAddress4()!=null)
	    	Address+=" "+bplocation.getC_Location().getAddress4().trim();
	    int length=100;
        int cstrlength=39;
        int cstrlength2=79;
        int cstrlength3=119;
        int cstrlength4=159;
	    if(Address.length()<=100){
	    	length=Address.length();
	    }
	    if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARI")==0){			    
	    	writer.write("j1RUC/CIP: "+((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()) + "\n");
	        writer.write("j2RAZON SOCIAL: " +bpartner.getName()+ "\n");	
	        /*if((auxname.length())>cstrlength){
	            writer.write("j3        " + auxname.substring(cstrlength,((auxname.length()>cstrlength2)?cstrlength2:auxname.length())) + "\n");
	        }	
	        if((auxname.length())>cstrlength2){
	            writer.write("j4        " + auxname.substring(cstrlength2,((auxname.length()>cstrlength3)?cstrlength3:auxname.length())) + "\n");
	        }
	        if((auxname.length())>cstrlength3){
	            writer.write("j5        " + auxname.substring(cstrlength3,((auxname.length()>cstrlength4)?cstrlength4:auxname.length())) + "\n");
	        }		    	
	    	*/		    
		    writer.write("j3"+Address.substring(0, length) + "\n");
		    writer.write("j4"+invoice.getDocumentNo() + "\n");    
	    }else{
	    	writer.write("jS" + bpartner.getName() + "\n");
		    writer.write("jR"+((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()) + "\n");
		    writer.write("j3"+Address.substring(0, length) + "\n");
		    writer.write("jF"+invoice.getDocumentNo() + "\n");
		    writer.write("j01"+invoice.getDocumentNo() + "\n");
		    //writer.write("jI"+ fiscalprint.getFiscalPrintSerial()+ "\n");   
		    
	    }
	    List<MInvoiceLine> invoicelinequery= new Query(getCtx(), MInvoiceLine.Table_Name, 
	    		MInvoiceLine.COLUMNNAME_C_Invoice_ID+"=?", get_TrxName()).setParameters(invoice.getC_Invoice_ID())
	    		.setOrderBy(MInvoiceLine.COLUMNNAME_Line)
	    		.list();
	    //NumberFormat f = NumberFormat.getInstance(Locale.ENGLISH);
	    //if (f instanceof DecimalFormat) {
	     //   ((DecimalFormat) f).setDecimalSeparatorAlwaysShown(true);
	    //}
	   // NumberFormat formatprice = NumberFormat.getInstance(Locale.ENGLISH);
	    //if (f instanceof DecimalFormat) {
	    //    ((DecimalFormat) formatprice).setDecimalSeparatorAlwaysShown(true);
	    //}
	    
	    String aux="";
	    for (MInvoiceLine invoiceline:invoicelinequery) {
	    	String tipodetasa=" ";
	    	String descripcion = " ";
	    	String codigo = null;
	    	//if(invoiceline.getM_Product().getName()!=null)
	    		//auxproductname = invoiceline.getM_Product().getName().replace("&amp;", "&").replace("&quot;", "\"").replace("&apos;", "\'");
	    	if(invoiceline.getM_Product_ID()==0 && invoiceline.getC_Charge_ID()==0) {
	    		writer.write("q-"+String.format(Locale.ENGLISH,"%07.2f", invoiceline.getPriceEntered()).replace(".", ""));
	    	}else {
		    	if(invoiceline.getM_Product_ID()!=0) {
		    
		    		codigo = "|" + invoiceline.getM_Product().getValue() + "|";
		    		descripcion= invoiceline.getM_Product().getName().substring(0, Math.min(invoiceline.getM_Product().getName().length(), 127));
		    	}else if(invoiceline.getC_Charge_ID()!=0) {
		    		descripcion= invoiceline.getC_Charge().getName().substring(0, Math.min(invoiceline.getC_Charge().getName().length(), 127));
		    	}
		    	
		    	if(invoiceline.getDescription()!=null && invoiceline.getM_Product().getName().length()<127) {
		    		
		    		descripcion=invoiceline.getDescription().substring(0, Math.min(invoiceline.getDescription().length(), 127));
		    	}
		    		
		    	
		    	if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARI")==0){
		    		log.log(Level.SEVERE, invoiceline.getC_Tax().getRate().toString());
		    		if(invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(7))==0)
		    			tipodetasa="!";
		    		else if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(10))==0)
	                    tipodetasa=String.valueOf((char)34);
		    		else if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(15))==0)
	                    tipodetasa=String.valueOf((char)35);
		    		
		    		//log.log(Level.SEVERE, tipodetasa.toString());	    		
		    		//writer.write("@Codigo: "+ invoiceline.getM_Product().getValue()+"\n");
		    		//String line =tipodetasa + String.format(Locale.ENGLISH,"%011.2f", invoiceline.getPriceEntered()).replace(".", "") 
			    	//		+ String.format(Locale.ENGLISH,"%09.3f",invoiceline.getQtyEntered()).replace(".", "");
		    		//if(codigo!=null)
		    		//	line = line + codigo;
		    		//writer.write( line + descripcion  + "\n");
		    	}else{
		    		tipodetasa="d0";
		    		if(invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(7))==0)
		    			tipodetasa="d1";
		    		else if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(10))==0)
	                    tipodetasa="d2";
		    		else if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(15))==0)
	                    tipodetasa="d3";
		    	}	
		    		String line =tipodetasa + String.format(Locale.ENGLISH,"%011.2f", invoiceline.getPriceEntered()).replace(".", "") 
			    			+ String.format(Locale.ENGLISH,"%09.3f",invoiceline.getQtyEntered()).replace(".", "");
		    		if(codigo!=null)
		    			line = line + codigo;
		    		writer.write( line + descripcion  + "\n");
		    		/*writer.write(tipodetasa + String.format(Locale.ENGLISH,"%011.2f", invoiceline.getPriceEntered()).replace(".", "") 
			    			+ String.format(Locale.ENGLISH,"%09.3f",invoiceline.getQtyEntered()).replace(".", "") + ((invoiceline.getM_Product().getName()!=null)?
							invoiceline.getM_Product().getName().substring(0, Math.min(invoiceline.getM_Product().getName().length(), 25)):
								" ")  + "\n");*/
	    	}			    					    
			
	    }
	    writer.write("j5Comercio: " +((bpartner.getName2()!=null)?bpartner.getName2():"")+ "\n");
	    writer.write("j6Termino de pago: " +((invoice.getC_PaymentTerm().getName()!=null)?invoice.getC_PaymentTerm().getName():"")+ "\n");
	    writer.write("101");
	    writer.flush();
	}
	
	public boolean HasarInvoice(MInvoice invoice) throws FileNotFoundException{
		File file=null;
		Hasar objHasar =new Hasar();
		try{
			file = new File("factura.txt");    
			if(file.exists())
				file.delete();
			file.createNewFile();
		    // creates a FileWriter Object
		    FileWriter writer = new FileWriter(file); 
		    // Writes the content to the file

		    MBPartner bpartner=new MBPartner(getCtx(), invoice.getC_BPartner_ID(), get_TrxName());
		    
		    char TD=objHasar.TD;
		    if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARC")==0){
		    	
		    
		    	TD='D';
		    	writer.append(objHasar.DF).append(objHasar.FS).append(bpartner.getName() + objHasar.FS + ((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()))
    			.append(objHasar.FS).append("COD:"+ bpartner.getValue().trim()).append(objHasar.FS).append(objHasar.NR).append(objHasar.FS).append(objHasar.FS).append(objHasar.FS)
    			.append(TD).append(objHasar.LF);	
		    }else{
		    	writer.append(objHasar.DF).append(objHasar.FS).append(bpartner.getName() + objHasar.FS + ((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()))
    			.append(objHasar.FS).append("COD:"+ bpartner.getValue().trim()).append(objHasar.FS).append(objHasar.NR).append(objHasar.FS)
    			.append(TD).append(objHasar.LF);
		    }
		    MBPartnerLocation bplocation=new MBPartnerLocation(getCtx(),invoice.getC_BPartner_Location_ID(),get_TrxName());
		    String Address=" ";
		    if(bplocation.getC_Location().getAddress1()!=null)
		    	Address=bplocation.getC_Location().getAddress1().trim();
		    if(bplocation.getC_Location().getAddress2()!=null)
		    	Address+=" "+bplocation.getC_Location().getAddress2().trim();
		    if(bplocation.getC_Location().getAddress3()!=null)
		    	Address+=" "+bplocation.getC_Location().getAddress3().trim();
		    if(bplocation.getC_Location().getAddress4()!=null)
		    	Address+=" "+bplocation.getC_Location().getAddress4().trim();
		    int length=100;
		    if(Address.length()<=100){
		    	length=Address.length();
		    }
		    //writer.append(objHasar.CED).append(objHasar.FS).append('1').append(objHasar.FS).append("Dir:"+Address.substring(0, length)).append(objHasar.LF);	
		    
		    List<MInvoiceLine> invoicelinequery= new Query(getCtx(), MInvoiceLine.Table_Name, 
		    		MInvoiceLine.COLUMNNAME_C_Invoice_ID+"=?", get_TrxName()).setParameters(invoice.getC_Invoice_ID()).list();
		    
		    NumberFormat f = NumberFormat.getInstance(Locale.ENGLISH);
		    if (f instanceof DecimalFormat) {
		        ((DecimalFormat) f).setDecimalSeparatorAlwaysShown(true);
		    }
		    String aux="";
		    for (MInvoiceLine invoiceline:invoicelinequery) {
		    	writer.append(objHasar.PI).append(objHasar.FS).append(((invoiceline.getM_Product().getName()!=null)?
		    																invoiceline.getM_Product().getName().subSequence(0,invoiceline.getM_Product().getName().length()):
		    																	" ")).append(objHasar.FS);
		    	aux=f.format(invoiceline.getQtyEntered());
		       	writer.append(aux.subSequence(0, aux.length())).append(objHasar.FS);
		       	aux=f.format(invoiceline.getPriceEntered());
		       	writer.append(aux.subSequence(0, aux.length())).append(objHasar.FS);
		       	aux=f.format(invoiceline.getC_Tax().getRate());
		       	writer.append(aux.subSequence(0, aux.length())).append(objHasar.FS);
    			writer.append('M').append(objHasar.FS)
    			.append(((invoiceline.getM_Product().getValue()!=null)?
    					invoiceline.getM_Product().getValue().subSequence(0,invoiceline.getM_Product().getValue().length()): " ")).append(objHasar.LF);
		    }
		    /*MInOut shipment = new Query(getCtx(), MInOut.Table_Name, "C_Order_ID=?",get_TrxName()).setParameters(invoice.getC_Order_ID()).first();
		    
		    
		    writer.append(objHasar.DOCF).append(objHasar.FS).append('1').append(objHasar.FS).append("CLIENTE: "+bpartner.getValue().trim().toUpperCase()+" - "+bpartner.getName().trim().toUpperCase()+" TELF: "+((bplocation.getPhone()==null)?"":bplocation.getPhone())+", "+((bplocation.getPhone()==null)?"":bplocation.getPhone())+" VENDEDOR: "+((bpartner.getSalesRep().getName()==null)?"":bpartner.getSalesRep().getName().trim().toUpperCase())).append(objHasar.LF);
		    writer.append(objHasar.DOCF).append(objHasar.FS).append('2').append(objHasar.FS).append("DIRECCION: "+Address.substring(0, length)).append(objHasar.LF);
		    writer.append(objHasar.DOCF).append(objHasar.FS).append('3').append(objHasar.FS).append("REGION DE VENTA: "+((bplocation.getC_SalesRegion().getName()==null)?"":bplocation.getC_SalesRegion().getName().trim())).append(objHasar.LF);
		    writer.append(objHasar.DOCF).append(objHasar.FS).append('4').append(objHasar.FS).append("ENTREGA: "+((shipment.getDocumentNo()==null)?"":shipment.getDocumentNo().trim().toUpperCase())+" REPARTO: "+((shipment.getM_Shipper().getName()==null)?"":shipment.getM_Shipper().getName().trim().toUpperCase())+" NRO FACTURA INTERNO: "+((invoice.getDocumentNo()==null)?"":invoice.getDocumentNo().trim().toUpperCase())+" "+Msg.getMsg(Env.getAD_Language(getCtx()) , "payment.terms")+": "+((invoice.getC_PaymentTerm().getName()==null)?"":invoice.getC_PaymentTerm().getName().trim().toUpperCase())).append(objHasar.LF);
		    */
		    
		    
		    
		    writer.append('E').append(objHasar.LF);
		    
		    writer.flush();
		    writer.close();
						
		 }
		 
        catch(Exception e){
            e.printStackTrace();
        }
		
		return true;
		
		
		
		
	}
	
	public boolean HasarZXReport(String ReportType){
		if(p_Date1!=null){
			Calendar date1=Calendar.getInstance();
			date1.setTimeInMillis(p_Date1.getTime());
			date1aux=String.format("%4d",date1.get(Calendar.YEAR)).substring(2, 4) +
								String.format("%02d",date1.get(Calendar.MONTH)+1) +
									String.format( "%02d",date1.get(Calendar.DAY_OF_MONTH));
			Calendar date2=Calendar.getInstance();
			date2.setTimeInMillis(p_Date2.getTime());
			date2aux=String.format("%4d",date2.get(Calendar.YEAR)).substring(2, 4) +
								String.format("%02d",date2.get(Calendar.MONTH)+1) +
									String.format( "%02d",date2.get(Calendar.DAY_OF_MONTH));
										
			System.out.println(date1aux);
			System.out.println(date2aux);
			return false;
		}
		File file=null;
		Hasar objHasar =new Hasar();
		try{
			file = new File("factura.txt");    
			if(file.exists())
				file.delete();
			file.createNewFile();
		    // creates a FileWriter Object
		    FileWriter writer = new FileWriter(file); 
		    // Writes the content to the file
		    if(ReportType.compareTo("Z")==0){
		    	writer.append('9').append(objHasar.FS).append('Z');
		    }else if (ReportType.compareTo("X")==0){
		    	writer.append('9').append(objHasar.FS).append('X');
		    }else {
		    	char i=161;
		    	writer.append(':').append(objHasar.FS).append(date1aux).append(objHasar.FS).append(date2aux).append(objHasar.FS).append('G');
		    }
		    writer.flush();
		    writer.close();
		}
	 
	    catch(Exception e){
	        e.printStackTrace();
	    }
		return true;
		
	}
	
	private boolean StarZXReport(String ReportType) {
		if(p_Date1!=null){
			Calendar date1=Calendar.getInstance();
			date1.setTimeInMillis(p_Date1.getTime());
			date1aux=String.format( "%02d",date1.get(Calendar.DAY_OF_MONTH)) +
						String.format("%02d",date1.get(Calendar.MONTH)+1) +
							String.format("%4d",date1.get(Calendar.YEAR)).substring(2, 4);
			
			Calendar date2=Calendar.getInstance();
			date2.setTimeInMillis(p_Date2.getTime());
			date2aux=String.format( "%02d",date2.get(Calendar.DAY_OF_MONTH)) +
						String.format("%02d",date2.get(Calendar.MONTH)+1) +	
							String.format("%4d",date2.get(Calendar.YEAR)).substring(2, 4) ;
										
			System.out.println(date1aux);
			System.out.println(date2aux);
			//return "";
		}
		File file=null;
		
		try{
			file = new File("factura.txt");    
			if(file.exists())
				file.delete();
			file.createNewFile();
		    // creates a FileWriter Object
		    FileWriter writer = new FileWriter(file); 
		    // Writes the content to the file
		    if(ReportType.compareTo("Z")==0){
		    	writer.append("I0Z");
		    }else if(ReportType.compareTo("X")==0){
		    	writer.append("I0X");
		    }else{
		    	writer.append("I2S" + date1aux + date2aux);
		    }
		    
		    writer.flush();
		    writer.close();
		}
		 
	    catch(Exception e){
	        e.printStackTrace();
	    }
			    return true;
	}
	
	

}
