package com.erpgs.fiscalprint.agrosilos.process;

import java.util.logging.Level;

import org.compiere.model.MInvoice;
import org.compiere.model.MOrder;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.erpgs.fiscalprint.agrosilos.model.MInvoiceFiscal;

public class ProcessInvoice extends SvrProcess{
	private int p_C_Invoice_ID;
	private String p_WasPrinted;
	private String p_fiscalNumber;
	private String p_DocumentNo;
	private String p_znumber="PENDIENTE";
	private String p_serialnumber="PENDIENTE";
	private int p_C_Invoice_Fiscal_ID;
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			
			if (para[i].getParameter() == null)
				;
			
			else if (name.equals("C_Invoice_ID")){
				p_C_Invoice_ID =  para[i].getParameterAsInt();							
			}		
			else if (name.equals("WasPrinted"))
				p_WasPrinted =  para[i].getParameterAsString();
			else if (name.equals("fiscalNumber"))
				p_fiscalNumber =  para[i].getParameterAsString();
			else if (name.equals("DocumentNo"))
				p_DocumentNo =  para[i].getParameterAsString();
			else if (name.equals("znumber"))
				p_znumber =  para[i].getParameterAsString();				
			else if (name.equals("serialnumber"))
				p_serialnumber =  para[i].getParameterAsString();
			else if (name.equals("C_Invoice_Fiscal_ID")){
				p_C_Invoice_Fiscal_ID =  para[i].getParameterAsInt();							
			}			
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}

	@Override
	protected String doIt() throws Exception {		
		
		MInvoiceFiscal fiscaldata;
		if(p_C_Invoice_Fiscal_ID > 0) {
			fiscaldata = new MInvoiceFiscal(getCtx(), p_C_Invoice_Fiscal_ID,get_TrxName());
	    	fiscaldata.setfiscalprint_serial(p_serialnumber);
	    	fiscaldata.setfiscal_zreport(p_znumber);
	    	fiscaldata.set_ValueOfColumn("WasPrinted",p_WasPrinted);
	    	fiscaldata.saveEx();
	    	return "ok";
		}
		MInvoice invoice = new MInvoice(getCtx(), p_C_Invoice_ID, get_TrxName());
		if(p_fiscalNumber.compareTo("-1")!=0)
			invoice.set_ValueOfColumn("fiscalNumber",p_fiscalNumber);
		if(p_DocumentNo.compareTo("-1")!=0)
			invoice.setDocumentNo(p_DocumentNo);
		if(p_znumber.compareTo("-1")!=0)
			invoice.set_ValueOfColumn("znumber",p_znumber);
		if(p_serialnumber.compareTo("-1")!=0)
			invoice.set_ValueOfColumn("serialnumber",p_serialnumber);
		
		invoice.set_ValueOfColumn("WasPrinted",p_WasPrinted);
		invoice.save();
		
		
		//Create Fiscal Info
		fiscaldata=new MInvoiceFiscal(getCtx(), 0,get_TrxName());
		MOrder order = new MOrder(getCtx(),invoice.getC_Order_ID(),get_TrxName());
        fiscaldata.setAD_Org_ID(order.getAD_Org_ID());
    	fiscaldata.setC_Order_ID(order.getC_Order_ID());
    	fiscaldata.setC_Invoice_ID(invoice.getC_Invoice_ID());
    	if(p_fiscalNumber.compareTo("-1")!=0)
    		fiscaldata.setfiscal_invoicenumber(p_fiscalNumber);
    	fiscaldata.setfiscalprint_serial(p_serialnumber);
    	fiscaldata.setfiscal_zreport(p_znumber);
    	fiscaldata.saveEx();
    	
		return "ok";
	}

	
}
