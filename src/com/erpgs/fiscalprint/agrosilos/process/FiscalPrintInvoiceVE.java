package com.erpgs.fiscalprint.agrosilos.process;

import java.io.*;
import java.math.BigDecimal;
import java.net.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MOrder;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.erpgs.fiscalprint.agrosilos.model.MFiscalPrintConf;
import com.erpgs.fiscalprint.agrosilos.model.MInvoiceFiscal;

public class FiscalPrintInvoiceVE extends SvrProcess{
	 private int p_C_Invoice_ID=0;
	 private int p_C_Order_ID=0;
	 private int p_C_FiscalPrintConf_ID=0;
	 private String p_ReportType=null;
	 private MFiscalPrintConf fiscalprint=null;
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_FiscalPrintConf_ID"))
				p_C_FiscalPrintConf_ID =  para[i].getParameterAsInt();
			else if (name.equals("C_Invoice_ID"))
				p_C_Invoice_ID =  para[i].getParameterAsInt();
			else if (name.equals("C_Order_ID"))
				p_C_Order_ID =  para[i].getParameterAsInt();
			else if (name.equals("ReportType"))
				p_ReportType =  para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		// TODO Auto-generated method stub
		fiscalprint=new MFiscalPrintConf(getCtx(), p_C_FiscalPrintConf_ID, get_TrxName());
		MInvoice invoice=null;
		MOrder order=null;
		if(p_C_Invoice_ID!=0)
			invoice =new MInvoice(getCtx(), p_C_Invoice_ID, get_TrxName());
		else if(p_C_Order_ID!=0){
			order=new MOrder(getCtx(),p_C_Order_ID,get_TrxName());
			invoice =new MInvoice(getCtx(), order.getC_Invoice_ID(), get_TrxName());
		}
		if(p_ReportType==null && invoice==null){
			return "La orden no posee factura asociada";
		}
		boolean check=false;
		if(p_ReportType!=null){
			if(fiscalprint.getFiscalPrintModel().compareTo("H")==0)
				check=HasarZXReport(p_ReportType);
			else if(fiscalprint.getFiscalPrintModel().compareTo("S")==0)
				check=StarZXReport(p_ReportType);;
		}else{
			if(fiscalprint.getFiscalPrintModel().compareTo("H")==0)
				check=HasarInvoice(invoice);
			else if(fiscalprint.getFiscalPrintModel().compareTo("S")==0)
				check=StarInvoice(invoice);
		}
		
		if(check){
		
		 try{
	            FileInputStream fis=new FileInputStream("factura.txt");
			 
	            /*creating a socket to send data to 192.168.1.123
	              i.e. IP address of the machine where the file is to be sent
	              and 10000 is the port number on which server listens  requests*/
	            Socket socket=new Socket(fiscalprint.getURL(),fiscalprint.getPort());
	            socket.setSoTimeout(10000);
	            OutputStream os=socket.getOutputStream();
	            int ch=0;
	            System.out.println("Sending file  to "+fiscalprint.getURL());
	            while(true){
	 
	                ch=fis.read();
	                if(ch==-1)
	                  break;
	                os.write(ch);
	            }
	            fis.close();
	            os.close();
	            System.out.println("Sending process completed");
	        }
	 
	        catch(Exception e){
	            e.printStackTrace();
	        }
		//invocamos la lectura de la informacion para que espere
		 	if(Receiver()){
			 	//leemos el archivo status.txt e insertamos los valores en la bd si se recibe exitosamente
		 		
			 	ProcessStatus(order,invoice);
		 	}else{
		 		return "Problema recibiendo la informacion fiscal";
		 	}
		}
		return "Ok";
	}
	public void ProcessStatus(MOrder order,MInvoice invoice) {
		// TODO Auto-generated method stub
		try{
			File fileStatus = new File("Status.txt");  
			FileReader reader = new FileReader(fileStatus);
	        BufferedReader br = new BufferedReader(reader); 
	        String status; 
	        while((status = br.readLine()) != null) {
	        	if(fiscalprint.getModelName().compareTo("SRP812")!=0){
	        		int zlength=status.substring(47,51).length();
		            int znumber=Integer.parseInt(status.substring(47,51))+1;
		            MInvoiceFiscal fiscaldata=new MInvoiceFiscal(getCtx(), 0,get_TrxName());
		            fiscaldata.setAD_Org_ID(order.getAD_Org_ID());
		        	fiscaldata.setC_Order_ID(order.getC_Order_ID());
		        	fiscaldata.setC_Invoice_ID(invoice.getC_Invoice_ID());
		        	if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARI")==0)
		        		fiscaldata.setfiscal_invoicenumber(status.substring(21,29));
		        	else
		        		fiscaldata.setfiscal_invoicenumber(status.substring(88,96));
		        	fiscaldata.setfiscalprint_serial(status.substring(66,76));
		        	fiscaldata.setfiscal_zreport(String.format("%0" + String.valueOf(zlength) +"d", znumber));
		        	fiscaldata.saveEx();
	        	}
	        	else{
	        		int zlength=status.substring(77,81).length();
		            int znumber=Integer.parseInt(status.substring(77,81))+1;
		            MInvoiceFiscal fiscaldata=new MInvoiceFiscal(getCtx(), 0,get_TrxName());
		            fiscaldata.setAD_Org_ID(order.getAD_Org_ID());
		        	fiscaldata.setC_Order_ID(order.getC_Order_ID());
		        	fiscaldata.setC_Invoice_ID(invoice.getC_Invoice_ID());
		        	if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARI")==0)
		        		fiscaldata.setfiscal_invoicenumber(status.substring(21,29));
		        	else
		        		fiscaldata.setfiscal_invoicenumber(status.substring(47,55));
		        	fiscaldata.setfiscalprint_serial(status.substring(92,102));
		        	fiscaldata.setfiscal_zreport(String.format("%0" + String.valueOf(zlength) +"d", znumber));
		        	fiscaldata.saveEx();	        		
	        	}	            
	        } 
	        reader.close();
	        fileStatus.delete();
		}catch(Exception e){
            e.printStackTrace();
        }
	}

	public Boolean Receiver(){
		try{
				//we are creating the socket on port 10000
	            ServerSocket server=new ServerSocket(11000);
	            System.out.println("Esperando archivo...");
	            server.setSoTimeout(60000);
	            Socket socket=server.accept();
	            InputStream is=socket.getInputStream();
	            FileOutputStream fos=new FileOutputStream("Status.txt");
	            int ch=0;
	            System.out.println("Almacenando contenido en Status.txt");
	            while(true){
	                ch=is.read();
	                if(ch==-1)
	                  break;
	                fos.write(ch);
	            }
	            is.close();
	            fos.close();
	            System.out.println("Creación de archivo completada.");
	            server.close();
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return false;
	        } catch(Exception e){
	            e.printStackTrace();
	            return false;
	       }
		   return true;
	}
	public boolean StarZXReport(String ReportType) {
		File file=null;
		
		try{
			file = new File("factura.txt");    
			if(file.exists())
				file.delete();
			file.createNewFile();
		    // creates a FileWriter Object
		    FileWriter writer = new FileWriter(file); 
		    // Writes the content to the file
		    if(ReportType.compareTo("Z")==0){
		    	writer.append("I0Z");
		    }else{
		    	writer.append("I0X");
		    }
		    writer.flush();
		    writer.close();
		}
		 
	    catch(Exception e){
	        e.printStackTrace();
	    }
			    return true;
	}

	public boolean StarInvoice(MInvoice invoice) {
		// TODO Auto-generated method stub
		/**
		 * 
		 * jSNOMBRE RAZON SOCIAL
			jRRUC_CEDULA
			j3LINEA 3 ENCABEZADO
			j4LINEA 4 ENCABEZADO
			j5LINEA 5 Encabezado
			j6LINEA 6
			@COMENTARIO
			!000000850000001000CHRISTIE`S 3825T/3826T TITANI+ME690
			p-2000
			!000000880000001000PROG. GENERICO
			p-2000
			!000000880000001000PROG. GENERICO
			p-2000
			@prueba de comentarios para que se muestr
			@ en la fiscal
			@  ***GARANTIA***
			101
		 * 
		 */
		
		File file=null;
		String auxname=null;
		String auxaddress="Barcelona";
		
		try{
			file = new File("factura.txt");    
			if(file.exists())
				file.delete();
			file.createNewFile();
		    // creates a FileWriter Object
		    FileWriter writer = new FileWriter(file); 
		    // Writes the content to the file

		    MBPartner bpartner=new MBPartner(getCtx(), invoice.getC_BPartner_ID(), get_TrxName());
		    MBPartnerLocation bplocation=new MBPartnerLocation(getCtx(),invoice.getC_BPartner_Location_ID(),get_TrxName());
		    String Address=" ";
	        
		    if(bplocation.getC_Location().getAddress1()!=null)
		    	Address=bplocation.getC_Location().getAddress1().trim();
		    if(bplocation.getC_Location().getAddress2()!=null)
		    	Address+=" "+bplocation.getC_Location().getAddress2().trim();
		    if(bplocation.getC_Location().getAddress3()!=null)
		    	Address+=" "+bplocation.getC_Location().getAddress3().trim();
		    if(bplocation.getC_Location().getAddress4()!=null)
		    	Address+=" "+bplocation.getC_Location().getAddress4().trim();
		    /*int length=100;
		    if(Address.length()<=100){
		    	length=Address.length();
		    }*/
		    if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARI")==0){
			      //formateamos para que la descripcion del cliente/direccion no pase de 40 caracteres
		        int cstrlength=39;
		        int cstrlength2=79;
		        int cstrlength3=119;
		        int cstrlength4=159;
		    	auxname = bpartner.getName().replace("&amp;", "&").replace("&quot;", "\"").replace("&apos;", "\'");			    
		        if(auxname.length()<=cstrlength){
		            cstrlength=auxname.length();			
		        }
		        writer.write("i00Nombre: " + auxname.substring(0,cstrlength) + "\n");
		        if((auxname.length())>cstrlength){
		            writer.write("i01        " + auxname.substring(cstrlength,((auxname.length()>cstrlength2)?cstrlength2:auxname.length())) + "\n");
		        }	
		        if((auxname.length())>cstrlength2){
		            writer.write("i02        " + auxname.substring(cstrlength2,((auxname.length()>cstrlength3)?cstrlength3:auxname.length())) + "\n");
		        }
		        if((auxname.length())>cstrlength3){
		            writer.write("i03        " + auxname.substring(cstrlength3,((auxname.length()>cstrlength4)?cstrlength4:auxname.length())) + "\n");
		        }
		        //writer.write("i01" + bpartner.getName() + "\n");
		        writer.write("i04C.I/RIF: " + ((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()) + "\n");
			    //writer.write("i02"+((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()) + "\n");

		        if(Address!=null){
		            if(Address.trim().compareTo("")!=0){
		                auxaddress=Address.trim();
		            }	        
			        if(auxaddress.length()<=cstrlength){
			            cstrlength=auxaddress.length();			
			        }
			        writer.write("i05Direccion: " + auxaddress.substring(0,cstrlength) + "\n");
			        if((auxaddress.length())>cstrlength){
			            writer.write("i06        " + auxaddress.substring(cstrlength,((auxaddress.length()>cstrlength2)?cstrlength2:auxaddress.length())) + "\n");
			        }	
			        if((auxaddress.length())>cstrlength2){
			            writer.write("i07        " + auxaddress.substring(cstrlength2,((auxaddress.length()>cstrlength3)?cstrlength3:auxaddress.length())) + "\n");
			        }
			        if((auxaddress.length())>cstrlength3){
			            writer.write("i08        " + auxaddress.substring(cstrlength3,((auxaddress.length()>cstrlength4)?cstrlength4:auxaddress.length())) + "\n");
			        }
				}
		        //writer.write("i03"+Address.substring(0, length) + "\n");
		        MOrder order = new MOrder(getCtx(), invoice.getC_Order_ID(), get_TrxName());
		        writer.write("i09Orden Nro.: " + order.getDocumentNo() + "\n");
			    
		    }else{
		    	
		    	writer.write("iS*" + bpartner.getName().substring(0, (bpartner.getName().length()>38)?38:bpartner.getName().length()) + "\n");
			    writer.write("iR*" + ((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()) + "\n");
			    /**buscamos el numero de la factura afectada**/
			    //obtenemos la orden de venta
			    MOrder devOrder = new MOrder(getCtx(),invoice.getC_Order_ID(),get_TrxName());
			    MInvoice afeInvoice = new  Query(Env.getCtx(),MInvoice.Table_Name,MInvoice.COLUMNNAME_C_Order_ID+" = ?", get_TrxName())
			    						.setParameters(devOrder.getRef_Order_ID()).first();
			    
			    System.out.println("Orden : ");
			    //System.out.println(pi.getRef_Order_ID());
			    String sql = "select * from " + MInvoiceFiscal.Table_Name + " where " + MInvoiceFiscal.COLUMNNAME_C_Order_ID +" = ? ";
			    PreparedStatement pstmt = null;
			    
				try
				{
					pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
					int index = 1;
						if (devOrder.getRef_Order_ID() != 0){ 
							pstmt.setInt(index++, devOrder.getRef_Order_ID());
						}
						
						
				}
				catch (Exception e)
				{
					log.log(Level.SEVERE, sql.toString(), e);
				}
				ResultSet rs = null;
				rs = pstmt.executeQuery();
				int C_Invoice_Fiscal_ID=0;
				while (rs.next ())
				{
					C_Invoice_Fiscal_ID=rs.getInt(MInvoiceFiscal.COLUMNNAME_C_Invoice_Fiscal_ID);
				}
			    
			    MInvoiceFiscal fiscalinvoice =  new MInvoiceFiscal(getCtx(), C_Invoice_Fiscal_ID, get_TrxName());
			    writer.write("iF" + ((fiscalinvoice.getfiscal_invoicenumber()!=null)?fiscalinvoice.getfiscal_invoicenumber():afeInvoice.getDocumentNo()) + "\n");
			    writer.write("iI"+ fiscalprint.getFiscalPrintSerial()+ "\n");
			    //String.format("%02d",due.get(Calendar.DAY_OF_MONTH))+"/"+String.format("%02d",due.get(Calendar.MONTH)+1)+"/"+String.format("%4d",due.get(Calendar.YEAR));
			    //Calendar due=Calendar.getInstance();
				//due.setTimeInMillis(invoice.getDateInvoiced().getTime());
			    Calendar invoiceDate=Calendar.getInstance();
			    invoiceDate.setTimeInMillis(afeInvoice.getDateInvoiced().getTime());
			    String date=String.format("%02d",invoiceDate.get(Calendar.DAY_OF_MONTH))+"/"+
			    			String.format("%02d",invoiceDate.get(Calendar.MONTH)+1)+"/"+
			    			String.format("%4d",invoiceDate.get(Calendar.YEAR)).substring(2,4);
			    writer.write("iD0"+ date+ "\n");
			    //agregamos un comentario con el numero de la orden
			    writer.write("i01Orden Nro.:"+ afeInvoice.getC_Order().getDocumentNo()+ "\n");
			    writer.write("i02Numero de documento:"+ invoice.getDocumentNo()+ "\n");
		    }
		    List<MInvoiceLine> invoicelinequery= new Query(getCtx(), MInvoiceLine.Table_Name, 
		    		MInvoiceLine.COLUMNNAME_C_Invoice_ID+"=?", get_TrxName()).setParameters(invoice.getC_Invoice_ID()).list();
		    //NumberFormat f = NumberFormat.getInstance(Locale.ENGLISH);
		    //if (f instanceof DecimalFormat) {
		     //   ((DecimalFormat) f).setDecimalSeparatorAlwaysShown(true);
		    //}
		   // NumberFormat formatprice = NumberFormat.getInstance(Locale.ENGLISH);
		    //if (f instanceof DecimalFormat) {
		    //    ((DecimalFormat) formatprice).setDecimalSeparatorAlwaysShown(true);
		    //}
		    
		    String aux="";
		    String itemprefix=" ";
		    for (MInvoiceLine invoiceline:invoicelinequery) {
		    	if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARI")==0){
		    		if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(16))==0 || invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(12))==0){
	                    itemprefix="!";
		    		}
		    		if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(8))==0){
	                    itemprefix=String.valueOf((char)34);
		    		}
		    		if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(31))==0){
	                    itemprefix=String.valueOf((char)35);
		    		}		    		
		    		writer.write("@Codigo: "+invoiceline.getM_Product().getValue().trim()+"\n");
		    		writer.write(itemprefix + String.format(Locale.ENGLISH,"%011.2f", invoiceline.getPriceEntered()).replace(".", "") 
		    			+ String.format(Locale.ENGLISH,"%09.3f",invoiceline.getQtyEntered()).replace(".", "") + ((invoiceline.getM_Product().getName()!=null)?
						invoiceline.getM_Product().getName().substring(0, Math.min(invoiceline.getM_Product().getName().length(), 25)):
							" ")  + "\n");
		    	}else{
		    		itemprefix="d0";
		    		if(invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(16))==0 || invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(12))==0)
		    			itemprefix="d1";
		    		else if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(8))==0)
	                    itemprefix="d2";
		    		else if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(31))==0)
	                    itemprefix="d3";
		    		writer.write(itemprefix + String.format(Locale.ENGLISH,"%011.2f", invoiceline.getPriceEntered()).replace(".", "") 
			    			+ String.format(Locale.ENGLISH,"%09.3f",invoiceline.getQtyEntered()).replace(".", "") +"|"+ 
		    				((invoiceline.getM_Product().getValue()!=null)?invoiceline.getM_Product().getValue().substring(0, Math.min(invoiceline.getM_Product().getValue().length(), 25)):
									" ")   +"|" + ((invoiceline.getM_Product().getName()!=null)?
							invoiceline.getM_Product().getName().substring(0, Math.min(invoiceline.getM_Product().getName().length(), 25)):
								" ")  + "\n");
		    	}
		    		
		    	
    			
		    }
		    writer.write("101");
		    writer.flush();
		    writer.close();
		    
		}
		catch(Exception e){
			e.printStackTrace();
        }
		
		return true;
	}

	public boolean HasarInvoice(MInvoice invoice) throws FileNotFoundException{
		File file=null;
		Hasar objHasar =new Hasar();
		try{
			file = new File("factura.txt");    
			if(file.exists())
				file.delete();
			file.createNewFile();
		    // creates a FileWriter Object
		    FileWriter writer = new FileWriter(file); 
		    // Writes the content to the file

		    MBPartner bpartner=new MBPartner(getCtx(), invoice.getC_BPartner_ID(), get_TrxName());
		    
		    char TD=objHasar.TD;
		    if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARC")==0){
		    	
		    
		    	TD='D';
		    	writer.append(objHasar.DF).append(objHasar.FS).append(bpartner.getName() + objHasar.FS + ((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()))
    			.append(objHasar.FS).append("COD:"+ bpartner.getValue().trim()).append(objHasar.FS).append(objHasar.NR).append(objHasar.FS).append(objHasar.FS).append(objHasar.FS)
    			.append(TD).append(objHasar.LF);	
		    }else{
		    	writer.append(objHasar.DF).append(objHasar.FS).append(bpartner.getName() + objHasar.FS + ((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()))
    			.append(objHasar.FS).append("COD:"+ bpartner.getValue().trim()).append(objHasar.FS).append(objHasar.NR).append(objHasar.FS)
    			.append(TD).append(objHasar.LF);
		    }
		    MBPartnerLocation bplocation=new MBPartnerLocation(getCtx(),invoice.getC_BPartner_Location_ID(),get_TrxName());
		    String Address=" ";
		    if(bplocation.getC_Location().getAddress1()!=null)
		    	Address=bplocation.getC_Location().getAddress1().trim();
		    if(bplocation.getC_Location().getAddress2()!=null)
		    	Address+=" "+bplocation.getC_Location().getAddress2().trim();
		    if(bplocation.getC_Location().getAddress3()!=null)
		    	Address+=" "+bplocation.getC_Location().getAddress3().trim();
		    if(bplocation.getC_Location().getAddress4()!=null)
		    	Address+=" "+bplocation.getC_Location().getAddress4().trim();
		    int length=100;
		    if(Address.length()<=100){
		    	length=Address.length();
		    }
		    //writer.append(objHasar.CED).append(objHasar.FS).append('1').append(objHasar.FS).append("Dir:"+Address.substring(0, length)).append(objHasar.LF);	
		    
		    List<MInvoiceLine> invoicelinequery= new Query(getCtx(), MInvoiceLine.Table_Name, 
		    		MInvoiceLine.COLUMNNAME_C_Invoice_ID+"=?", get_TrxName()).setParameters(invoice.getC_Invoice_ID()).list();
		    
		    NumberFormat f = NumberFormat.getInstance(Locale.ENGLISH);
		    if (f instanceof DecimalFormat) {
		        ((DecimalFormat) f).setDecimalSeparatorAlwaysShown(true);
		    }
		    String aux="";
		    for (MInvoiceLine invoiceline:invoicelinequery) {
		    	writer.append(objHasar.PI).append(objHasar.FS).append(((invoiceline.getM_Product().getName()!=null)?
		    																invoiceline.getM_Product().getName().subSequence(0,invoiceline.getM_Product().getName().length()):
		    																	" ")).append(objHasar.FS);
		    	aux=f.format(invoiceline.getQtyEntered());
		       	writer.append(aux.subSequence(0, aux.length())).append(objHasar.FS);
		       	aux=f.format(invoiceline.getPriceEntered());
		       	writer.append(aux.subSequence(0, aux.length())).append(objHasar.FS);
		       	aux=f.format(invoiceline.getC_Tax().getRate());
		       	writer.append(aux.subSequence(0, aux.length())).append(objHasar.FS);
    			writer.append('M').append(objHasar.FS)
    			.append(((invoiceline.getM_Product().getValue()!=null)?
    					invoiceline.getM_Product().getValue().subSequence(0,invoiceline.getM_Product().getValue().length()): " ")).append(objHasar.LF);
		    }
		    MInOut shipment = new Query(getCtx(), MInOut.Table_Name, "C_Order_ID=?",get_TrxName()).setParameters(invoice.getC_Order_ID()).first();
		    
		    
		    writer.append(objHasar.DOCF).append(objHasar.FS).append('1').append(objHasar.FS).append("CLIENTE: "+bpartner.getValue().trim().toUpperCase()+" - "+bpartner.getName().trim().toUpperCase()+" TELF: "+((bplocation.getPhone()==null)?"":bplocation.getPhone())+", "+((bplocation.getPhone()==null)?"":bplocation.getPhone())+" VENDEDOR: "+((bpartner.getSalesRep().getName()==null)?"":bpartner.getSalesRep().getName().trim().toUpperCase())).append(objHasar.LF);
		    writer.append(objHasar.DOCF).append(objHasar.FS).append('2').append(objHasar.FS).append("DIRECCION: "+Address.substring(0, length)).append(objHasar.LF);
		    writer.append(objHasar.DOCF).append(objHasar.FS).append('3').append(objHasar.FS).append("REGION DE VENTA: "+((bplocation.getC_SalesRegion().getName()==null)?"":bplocation.getC_SalesRegion().getName().trim())).append(objHasar.LF);
		    writer.append(objHasar.DOCF).append(objHasar.FS).append('4').append(objHasar.FS).append("ENTREGA: "+((shipment.getDocumentNo()==null)?"":shipment.getDocumentNo().trim().toUpperCase())+" REPARTO: "+((shipment.getM_Shipper().getName()==null)?"":shipment.getM_Shipper().getName().trim().toUpperCase())+" NRO FACTURA INTERNO: "+((invoice.getDocumentNo()==null)?"":invoice.getDocumentNo().trim().toUpperCase())+" "+Msg.getMsg(Env.getAD_Language(getCtx()) , "payment.terms")+": "+((invoice.getC_PaymentTerm().getName()==null)?"":invoice.getC_PaymentTerm().getName().trim().toUpperCase())).append(objHasar.LF);
		    
		    
		    
		    
		    writer.append('E').append(objHasar.LF);
		    
		    writer.flush();
		    writer.close();
						
		 }
		 
        catch(Exception e){
            e.printStackTrace();
        }
		
		return true;
		
		
		
		
	}
	
	public boolean HasarZXReport(String ReportType){
		File file=null;
		Hasar objHasar =new Hasar();
		try{
			file = new File("factura.txt");    
			if(file.exists())
				file.delete();
			file.createNewFile();
		    // creates a FileWriter Object
		    FileWriter writer = new FileWriter(file); 
		    // Writes the content to the file
		    if(ReportType.compareTo("Z")==0){
		    	writer.append('9').append(objHasar.FS).append('Z');
		    }else{
		    	writer.append('9').append(objHasar.FS).append('X');
		    }
		    writer.flush();
		    writer.close();
	 }
	 
    catch(Exception e){
        e.printStackTrace();
    }
		    return true;
		
	}
	
	

}
