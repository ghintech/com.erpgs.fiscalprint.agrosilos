package com.erpgs.fiscalprint.agrosilos.component;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;

import com.erpgs.fiscalprint.agrosilos.process.FiscalPrintInvoice;
import com.erpgs.fiscalprint.agrosilos.process.FiscalPrintInvoiceVE;
import com.erpgs.fiscalprint.agrosilos.process.FiscalPrintInvoiceVE_WS;
import com.erpgs.fiscalprint.agrosilos.process.FiscalPrintInvoiceWS;
import com.erpgs.fiscalprint.agrosilos.process.FiscalPrintInvoiceWSAgro;
import com.erpgs.fiscalprint.agrosilos.process.ProcessInvoice;


public class ProcessFactory implements IProcessFactory {

	@Override
	public ProcessCall newProcessInstance(String className) {
		// TODO Auto-generated method stub
		if(className.equals("com.erpgs.fiscalprint.agrosilos.process.FiscalPrintInvoice"))
			return new FiscalPrintInvoice();
		if(className.equals("com.erpgs.fiscalprint.agrosilos.process.FiscalPrintInvoiceVE"))
			return new FiscalPrintInvoiceVE();
		if(className.equals("com.erpgs.fiscalprint.agrosilos.process.FiscalPrintInvoiceWS"))
			return new FiscalPrintInvoiceWS();
		if(className.equals("com.erpgs.fiscalprint.agrosilos.process.FiscalPrintInvoiceWSAgro"))
			return new FiscalPrintInvoiceWSAgro();
		if(className.equals("com.erpgs.fiscalprint.agrosilos.process.ProcessInvoice"))
			return new ProcessInvoice();
		if(className.equals("com.erpgs.fiscalprint.agrosilos.process.FiscalPrintInvoiceVE_WS"))
			return new FiscalPrintInvoiceVE_WS();
		return null;
	}

}
