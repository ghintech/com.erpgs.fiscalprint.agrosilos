package com.erpgs.fiscalprint.agrosilos.component;

import java.sql.ResultSet;

import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;

import com.erpgs.fiscalprint.agrosilos.model.MFiscalPrintConf;
import com.erpgs.fiscalprint.agrosilos.model.MFiscalReportLog;
import com.erpgs.fiscalprint.agrosilos.model.MInvoiceFiscal;

public class ModelFactory implements IModelFactory{

	@Override
	public Class<?> getClass(String tableName) {
		// TODO Auto-generated method stub
		if(MFiscalPrintConf.Table_Name.equals(tableName))
			return MFiscalPrintConf.class;
		if(MFiscalReportLog.Table_Name.equals(tableName))
			return MFiscalReportLog.class;
		if(MInvoiceFiscal.Table_Name.equals(tableName))
			return MInvoiceFiscal.class;
		
		return null;
	}

	@Override
	public PO getPO(String tableName, int Record_ID, String trxName) {
		// TODO Auto-generated method stub
		if(MFiscalPrintConf.Table_Name.equals(tableName))
			return new MFiscalPrintConf(Env.getCtx(), Record_ID, trxName);
		if(MFiscalReportLog.Table_Name.equals(tableName))
			return new MFiscalReportLog(Env.getCtx(), Record_ID, trxName);
		if(MInvoiceFiscal.Table_Name.equals(tableName))
			return new MInvoiceFiscal(Env.getCtx(), Record_ID, trxName);
		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
		// TODO Auto-generated method stub
		if(MFiscalPrintConf.Table_Name.equals(tableName))
			return new MFiscalPrintConf(Env.getCtx(), rs, trxName);
		if(MFiscalReportLog.Table_Name.equals(tableName))
			return new MFiscalReportLog(Env.getCtx(), rs, trxName);
		if(MInvoiceFiscal.Table_Name.equals(tableName))
			return new MInvoiceFiscal(Env.getCtx(), rs, trxName);
		return null;
	}

}
