/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.erpgs.fiscalprint.agrosilos.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for C_FiscalReportLog
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_C_FiscalReportLog extends PO implements I_C_FiscalReportLog, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180521L;

    /** Standard Constructor */
    public X_C_FiscalReportLog (Properties ctx, int C_FiscalReportLog_ID, String trxName)
    {
      super (ctx, C_FiscalReportLog_ID, trxName);
      /** if (C_FiscalReportLog_ID == 0)
        {
			setC_FiscalReportLog_ID (0);
        } */
    }

    /** Load Constructor */
    public X_C_FiscalReportLog (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_FiscalReportLog[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Fiscal Report Log.
		@param C_FiscalReportLog_ID Fiscal Report Log	  */
	public void setC_FiscalReportLog_ID (int C_FiscalReportLog_ID)
	{
		if (C_FiscalReportLog_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_FiscalReportLog_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_FiscalReportLog_ID, Integer.valueOf(C_FiscalReportLog_ID));
	}

	/** Get Fiscal Report Log.
		@return Fiscal Report Log	  */
	public int getC_FiscalReportLog_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_FiscalReportLog_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set C_FiscalReportLog_UU.
		@param C_FiscalReportLog_UU C_FiscalReportLog_UU	  */
	public void setC_FiscalReportLog_UU (String C_FiscalReportLog_UU)
	{
		set_Value (COLUMNNAME_C_FiscalReportLog_UU, C_FiscalReportLog_UU);
	}

	/** Get C_FiscalReportLog_UU.
		@return C_FiscalReportLog_UU	  */
	public String getC_FiscalReportLog_UU () 
	{
		return (String)get_Value(COLUMNNAME_C_FiscalReportLog_UU);
	}

	public org.compiere.model.I_C_Invoice getC_Invoice() throws RuntimeException
    {
		return (org.compiere.model.I_C_Invoice)MTable.get(getCtx(), org.compiere.model.I_C_Invoice.Table_Name)
			.getPO(getC_Invoice_ID(), get_TrxName());	}

	/** Set Invoice.
		@param C_Invoice_ID 
		Invoice Identifier
	  */
	public void setC_Invoice_ID (int C_Invoice_ID)
	{
		if (C_Invoice_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Invoice_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Invoice_ID, Integer.valueOf(C_Invoice_ID));
	}

	/** Get Invoice.
		@return Invoice Identifier
	  */
	public int getC_Invoice_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Invoice_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Fiscal Print Status.
		@param FiscalPrintStatus Fiscal Print Status	  */
	public void setFiscalPrintStatus (String FiscalPrintStatus)
	{
		set_Value (COLUMNNAME_FiscalPrintStatus, FiscalPrintStatus);
	}

	/** Get Fiscal Print Status.
		@return Fiscal Print Status	  */
	public String getFiscalPrintStatus () 
	{
		return (String)get_Value(COLUMNNAME_FiscalPrintStatus);
	}

	/** Z Report = Z */
	public static final String REPORTTYPE_ZReport = "Z";
	/** X Report = X */
	public static final String REPORTTYPE_XReport = "X";
	/** Set Report Type.
		@param ReportType Report Type	  */
	public void setReportType (String ReportType)
	{

		set_Value (COLUMNNAME_ReportType, ReportType);
	}

	/** Get Report Type.
		@return Report Type	  */
	public String getReportType () 
	{
		return (String)get_Value(COLUMNNAME_ReportType);
	}

	/** Set Script.
		@param Script 
		Dynamic Java Language Script to calculate result
	  */
	public void setScript (String Script)
	{
		set_Value (COLUMNNAME_Script, Script);
	}

	/** Get Script.
		@return Dynamic Java Language Script to calculate result
	  */
	public String getScript () 
	{
		return (String)get_Value(COLUMNNAME_Script);
	}
}