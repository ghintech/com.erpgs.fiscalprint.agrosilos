package com.erpgs.fiscalprint.agrosilos.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MInvoiceFiscal extends X_C_Invoice_Fiscal {
	/**
	 *Fiscal invoice table cover all values about fiscal number, z report and other values 
	 */
	private static final long serialVersionUID = -4202747796705899428L;
	public MInvoiceFiscal(Properties ctx, int C_Invoice_Fiscal_ID,
			String trxName) {
		super(ctx, C_Invoice_Fiscal_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	public MInvoiceFiscal(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
}
