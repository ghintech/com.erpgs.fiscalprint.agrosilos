/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.erpgs.fiscalprint.agrosilos.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Model for C_FiscalPrintConf
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_C_FiscalPrintConf extends PO implements I_C_FiscalPrintConf, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180521L;

    /** Standard Constructor */
    public X_C_FiscalPrintConf (Properties ctx, int C_FiscalPrintConf_ID, String trxName)
    {
      super (ctx, C_FiscalPrintConf_ID, trxName);
      /** if (C_FiscalPrintConf_ID == 0)
        {
			setC_FiscalPrintConf_ID (0);
			setName (null);
			setPort (0);
			setURL (null);
        } */
    }

    /** Load Constructor */
    public X_C_FiscalPrintConf (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_FiscalPrintConf[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Fiscal Print Configuration.
		@param C_FiscalPrintConf_ID Fiscal Print Configuration	  */
	public void setC_FiscalPrintConf_ID (int C_FiscalPrintConf_ID)
	{
		if (C_FiscalPrintConf_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_FiscalPrintConf_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_FiscalPrintConf_ID, Integer.valueOf(C_FiscalPrintConf_ID));
	}

	/** Get Fiscal Print Configuration.
		@return Fiscal Print Configuration	  */
	public int getC_FiscalPrintConf_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_FiscalPrintConf_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set C_FiscalPrintConf_UU.
		@param C_FiscalPrintConf_UU C_FiscalPrintConf_UU	  */
	public void setC_FiscalPrintConf_UU (String C_FiscalPrintConf_UU)
	{
		set_Value (COLUMNNAME_C_FiscalPrintConf_UU, C_FiscalPrintConf_UU);
	}

	/** Get C_FiscalPrintConf_UU.
		@return C_FiscalPrintConf_UU	  */
	public String getC_FiscalPrintConf_UU () 
	{
		return (String)get_Value(COLUMNNAME_C_FiscalPrintConf_UU);
	}

	/** HASAR = H */
	public static final String FISCALPRINTMODEL_HASAR = "H";
	/** STAR = S */
	public static final String FISCALPRINTMODEL_STAR = "S";
	/** Set Fiscal Print Model.
		@param FiscalPrintModel Fiscal Print Model	  */
	public void setFiscalPrintModel (String FiscalPrintModel)
	{

		set_Value (COLUMNNAME_FiscalPrintModel, FiscalPrintModel);
	}

	/** Get Fiscal Print Model.
		@return Fiscal Print Model	  */
	public String getFiscalPrintModel () 
	{
		return (String)get_Value(COLUMNNAME_FiscalPrintModel);
	}

	/** Set Fiscal Print Serial.
		@param FiscalPrintSerial Fiscal Print Serial	  */
	public void setFiscalPrintSerial (String FiscalPrintSerial)
	{
		set_Value (COLUMNNAME_FiscalPrintSerial, FiscalPrintSerial);
	}

	/** Get Fiscal Print Serial.
		@return Fiscal Print Serial	  */
	public String getFiscalPrintSerial () 
	{
		return (String)get_Value(COLUMNNAME_FiscalPrintSerial);
	}

	/** Set ModelName.
		@param ModelName ModelName	  */
	public void setModelName (String ModelName)
	{
		set_Value (COLUMNNAME_ModelName, ModelName);
	}

	/** Get ModelName.
		@return ModelName	  */
	public String getModelName () 
	{
		return (String)get_Value(COLUMNNAME_ModelName);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Port.
		@param Port Port	  */
	public void setPort (int Port)
	{
		set_Value (COLUMNNAME_Port, Integer.valueOf(Port));
	}

	/** Get Port.
		@return Port	  */
	public int getPort () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Port);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set URL.
		@param URL 
		Full URL address - e.g. http://www.idempiere.org
	  */
	public void setURL (String URL)
	{
		set_Value (COLUMNNAME_URL, URL);
	}

	/** Get URL.
		@return Full URL address - e.g. http://www.idempiere.org
	  */
	public String getURL () 
	{
		return (String)get_Value(COLUMNNAME_URL);
	}
}